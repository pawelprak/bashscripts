#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------
# author:           Paweł Karp       
# description:      config for toolchain
#
# wersje
#--------------------------------------------------------------------------------------------------------------------------------
#   1.0 / 2016-01-07
#       -   start
#--------------------------------------------------------------------------------------------------------------------------------


CLEAN_SOURCES="no"       # !!! Uważać z tym, potem jest bardzo dużo ściągania danych z sieci!! yes/no - czy wszystkie źródła mają być ściągnięte od nowa
SYNC_CLOCK="no"          # yes/no - synchronizacja zegara z serwerem ntp
USEALLCORES="yes"        # yes/no - ile rdzeni procesora ma być wykorzystane
MENUCONFIG="no"          # czy zrobić ręczne ustawienie parametrów dla kernela

COMPILE_UBOOT="no"
WWW_UBOOT="https://github.com/RobertCNelson/u-boot" # adres kodów źródłowych dla bootloadera
UBOOT_DIR=$SOURCES_DIR/u-boot


WWW_SUNXI="https://github.com/jwrdegoede/u-boot-sunxi.git"
SUNXI_DIR=$SOURCES_DIR/sunxi

WWW_KERNEL="git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git"
KERNEL_DIR=$SOURCES_DIR/kernel

WWW_SUNXITOOLS="https://github.com/linux-sunxi/sunxi-tools.git"
SUNXITOOLS_DIR=$SOURCES_DIR/sunxitools



