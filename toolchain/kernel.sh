#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------
# autor:    Paweł Karp       
# opis:     przygotowanie kernela
#
# wersje:
#--------------------------------------------------------------------------------------------------------------------------------
# 1.0 / 2016-01-18
#   - start
#--------------------------------------------------------------------------------------------------------------------------------

scriptName=$(basename $BASH_SOURCE) 
logAndDisplay "Uruchomienie skryptu" "info" $scriptName $LOG_FILE

#--------------------------------------------------------------------------------------------------------------------------------
# ściągnięcie kodu kernela
#--------------------------------------------------------------------------------------------------------------------------------
temp="Kernel"
logAndDisplay "Ściągam $temp"  "info" $scriptName $LOG_FILE
downloadGit $WWW_KERNEL $KERNEL_DIR
case "$?" in
  "0") logAndDisplay "Pobrano - $temp" "ok" $scriptName $LOG_FILE ;;
  "1") logAndDisplay "Błąd pobrania plików - $temp" "error" $scriptName $LOG_FILE ;;
  "2") logAndDisplay "Już pobrane - $temp" "ok" $scriptName $LOG_FILE ;;
  *) logAndDisplay "Zła odpowiedź skryptu downloadGit.sh" "warn" $scriptName $LOG_FILE
esac



#--------------------------------------------------------------------------------------------------------------------------------
# ściągnięcie sunxi-tools
#--------------------------------------------------------------------------------------------------------------------------------
temp="Sunxi-Tools"
logAndDisplay "Ściągam $temp"  "info" $scriptName $LOG_FILE
downloadGit $WWW_SUNXITOOLS $SUNXITOOLS_DIR
case "$?" in
  "0") logAndDisplay "Pobrano - $temp" "ok" $scriptName $LOG_FILE ;;
  "1") logAndDisplay "Błąd plików - $temp" "error" $scriptName $LOG_FILE ;;
  "2") logAndDisplay "Już pobrane - $temp" "ok" $scriptName $LOG_FILE ;;
  *) logAndDisplay "Zła odpowiedź skryptu downloadGit.sh" "warn" $scriptName $LOG_FILE
esac




#--------------------------------------------------------------------------------------------------------------------------------
# kompilacja kernela
#--------------------------------------------------------------------------------------------------------------------------------
logAndDisplay "Rozpoczynam kompilację kernela..." "info" $scriptName $LOG_FILE
cd $KERNEL_DIR

make CROSS_COMPILE=arm-linux-gnueabihf- clean 

if [ ! -e $KERNEL_DIR/.config ]; then
    wget -O $KERNEL_DIR/.config https://raymii.org/s/inc/software/olimex/linux-sunxi-next.config.txt
    logAndDisplay "Ściągam plik .config" "ok" $scriptName $LOG_FILE
else
    logAndDisplay "Plik .config już istnieje" "ok" $scriptName $LOG_FILE
fi

# ręczna konfiguracja elementów kernela?
if [ $MENUCONFIG = "yes"]; then
    make -j1 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- menuconfig
    logAndDisplay "Zakończono konfigurację ręczną" "ok" $scriptName $LOG_FILE
fi

#kompilacja 
make -j1 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- all zImage modules_prepare
make -j1 deb-pkg KDEB_PKGVERSION=1.5 LOCALVERSION=-micro KBUILD_DEBARCH=armhf ARCH=arm 'DEBFULLNAME=Kopex' DEBEMAIL=pawel.karp@kopex.com.pl CROSS_COMPILE=arm-linux-gnueabihf-

#sprawdzenie wyjścia
if [ $? == 0 ]; then 
    logAndDisplay "Kompilacja zakończona" "ok" $scriptName $LOG_FILE
else
    logAndDisplay "Błąd kompilacji: $? " "error" $scriptName $LOG_FILE
    exit 1
fi


#--------------------------------------------------------------------------------------------------------------------------------
# pakowanie kernela
#--------------------------------------------------------------------------------------------------------------------------------
# spakowanie binarki i przeniesienie archiwum do katalogu output
logAndDisplay "Pakuję kernel" "info" $scriptName $LOG_FILE
mkdir -p $OUTPUT_DIR/kernel
cd $SOURCES_DIR
tar -cPf $OUTPUT_DIR/kernel/4.4.0-micro2.tar linux-headers-* linux-image-* linux-libc-dev*
































