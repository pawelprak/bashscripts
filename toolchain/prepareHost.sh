#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------
# autor:    Paweł Karp       
# opis:     przygotowanie komputera (laptopa) na którym ma być kompilowany kernel
#
# wersja:
#--------------------------------------------------------------------------------------------------------------------------------
# 1.0 / 2016-01-08
#   - start
#--------------------------------------------------------------------------------------------------------------------------------


logAndDisplay "Uruchomienie skryptu" "info" "prepareHost.sh" $LOG_FILE

PACKAGES="ntpdate dos2unix debconf-utils pv bc lzop zip binfmt-support bison build-essential ccache debootstrap flex gawk \
    crossbuild-essential-armhf gcc-arm-linux-gnueabihf qemu-user-static u-boot-tools uuid-dev zlib1g-dev unzip libusb-1.0-0-dev \
    parted pkg-config expect libncurses5-dev screen device-tree-compiler"


#--------------------------------------------------------------------------------------------------------------------------------
# Przygotowanie wpisów dla cross kompilera dla debian jessie
#--------------------------------------------------------------------------------------------------------------------------------
if [[ ! -f /etc/apt/sources.list.d/crosstools.list ]]; then
    logAndDisplay "Dodaję repozytorium dla jessie" "info" "prepareHost.sh"  $LOG_FILE

    dpkg --add-architecture armhf > /dev/null 2>&1
    echo 'deb http://emdebian.org/tools/debian/ jessie main' > /etc/apt/sources.list.d/crosstools.list
    wget 'http://emdebian.org/tools/debian/emdebian-toolchain-archive.key' -O - | apt-key add - >/dev/null
    
    logAndDisplay "Uruchamiam apt-get update" "info" "prepareHost.sh"  $LOG_FILE
    apt-get update -y
else
    logAndDisplay "Repozytorium dla jessie istnieje" "info" "prepareHost.sh"   $LOG_FILE
fi



#--------------------------------------------------------------------------------------------------------------------------------
# Instalacja pakietów
#--------------------------------------------------------------------------------------------------------------------------------
for packet in $PACKAGES; do

    installPackage $packet
    case "$?" in
      "0") logAndDisplay "Zainstalowano pakiet - $packet" "ok" "prepareHost.sh" $LOG_FILE ;;
      "1") logAndDisplay "Błąd instalacji pakietu - $packet" "error" "prepareHost.sh" $LOG_FILE ;;
      "2") logAndDisplay "Pakiet już zainstalowany - $packet" "info" "prepareHost.sh" $LOG_FILE ;;
      *) logAndDisplay "Zła odpowiedźskryptu installPackage.sh" "warn" "prepareHost.sh" $LOG_FILE
    esac

#    if [ $(dpkg-query -W -f='${Status}' $packet 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
#        #apt-get install -qq -y --no-install-recommends $PAK >/dev/null 2>&1
#        apt-get install -y --no-install-recommends $packet
#
#        if [ $? == 0 ]; then
#            logAndDisplay "Zainstalowano pakiet - $packet" "ok" "prepareHost.sh" $LOG_FILE
#        else
#            logAndDisplay "Błąd instalacji pakietu - $packet" "error" "prepareHost.sh" $LOG_FILE
#        fi
#    else
#        logAndDisplay "Pakiet już zainstalowany - $packet" "info" "prepareHost.sh" $LOG_FILE
#    fi
done









