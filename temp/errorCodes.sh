#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------
# author:           Paweł Karp       
# description:      standard error codes
#
# version:
#--------------------------------------------------------------------------------------------------------------------------------
#   1.0 / 2015-12-21
#       -   start
#--------------------------------------------------------------------------------------------------------------------------------


E_USAGE=64	    # The command was used incorrectly, e.g., with the wrong number of arguments, a bad flag, a bad syntax in a parameter, or whatever.
E_DATAERR=65    # The input data was incorrect in some way.  This should only be used for user's data and not system files.
E_NOINPUT=66	# An input file (not a system file) did not exist or was not readable.  This could also include errors like ``No message'' to a mailer (if it cared to catch it).
E_NOPERM=77	    # You did not have sufficient permission to perform the operation.  This is not intended for file system problems, which should use EX_NOINPUT or EX_CANTCREAT, but rather for higher level permissions.
E_NOTROOT=87    # Non-root exit error.