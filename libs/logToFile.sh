#!/bin/bash
scriptName=$(basename $BASH_SOURCE) 
#echo "load: $scriptName ..."

#--------------------------------------------------------------------------------------------------------------------------------
# author:           Paweł Karp       
# description:      log messages to specified file
#
# version
#--------------------------------------------------------------------------------------------------------------------------------
#   1.0 / 2015-12-18
#       -   start
#--------------------------------------------------------------------------------------------------------------------------------



#--------------------------------------------------------------------------------------------------------------------------------
# In:
#	$1    <-  text to log
#	$2    <-  log file path and name
#   $3    <-  "start" new log, backup old one. If argument not specified, logs are added to $2 file    
# Out:
#	0            ->  ok 
#   E_USAGE      ->  error
#--------------------------------------------------------------------------------------------------------------------------------
logToFile()
{

    E_USAGE=64
    date=`date +%Y-%m-%d`
    time=`date +%H:%M:%S`

    # Test whether text argument is present (non-empty).
    if [ -z "$1" ]; then # No command-line arguments
        echo "*** logToFile *** Must be at least one argument!"
        exit $E_USAGE
    fi

    if [ -e "$2" ]; then            # file exists             
        if [ -n "$3" ] && [ $3 == "start" ]; then  # Test whether command-line argument is present (non-empty) && order for new log file
            echo "*** logToFile *** - backing up existing log file"
            mv $2 $2"Old"           # backup existing file
        else                        
            echo "  " $time "-" $1 >> $2  # add text to existing file and exit
            return 0
        fi
    fi

    # create file if starting new log
    touch $2               
    echo "*** logToFile *** - creating new log file"
    echo "Start: " $date >> $2          # add text
    echo "  " $time "-" $1 >> $2        # add text
}



