#!/bin/bash
scriptName=$(basename $BASH_SOURCE) 
#echo "load: $scriptName ..."

#--------------------------------------------------------------------------------------------------------------------------------
# author:           Paweł Karp       
# description:      print formatted text to console
#
# version
#--------------------------------------------------------------------------------------------------------------------------------
#   1.0 / 2015-12-18
#       -   start
#--------------------------------------------------------------------------------------------------------------------------------





#--------------------------------------------------------------------------------------------------------------------------------
# In:
#	$1    <-  text
#	$2    <-  status: ok / info / warn / error
#   #3    <-  source (calling script name, host...)    
# Out:
#	0     ->  ok 
#	64    ->  error 
#--------------------------------------------------------------------------------------------------------------------------------
displayAlert()
{

    E_USAGE=64
    text=""

    # Test whether text argument is present (non-empty).
    if [ -z "$1" ]; then # No command-line arguments
        echo "*** displayAlert *** Must be at least one argument!"
        exit $E_USAGE
    fi

    # status parameter exists
    if [ -n "$2" ]; then                                     
        case "$2" in
            "ok")  text=$text"\e[32m[ $2\t]\x1B[0m" ;;       # 32m - green
            "error")  text=$text"\e[31m[ $2\t]\x1B[0m" ;;    # 31m - red
            "warn")  text=$text"\e[33m[ $2\t]\x1B[0m" ;;     # 33m - yellow
            "info")  text=$text"\e[36m[ $2\t]\x1B[0m" ;;     # 36m - cyan
            *) text=$text"\e[32m  \t \x1B[0m" ;;             # default
        esac
    else
        text=$text"\e[32m  \t \x1B[0m"
    fi

    # add text
    text=$text" $1 "  

    # source parameter exists
    if [ -n "$3" ]; then                    
        text=$text"\e[95m[ $3 ]\x1B[0m"     # 95m - light magenta
    fi

    # print to console
    echo -e $text       #   send text to console
    sleep .25            #   user friendly
}
