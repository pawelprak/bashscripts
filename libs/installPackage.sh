#!/bin/bash
scriptName=$(basename $BASH_SOURCE) 
#echo "load: $scriptName ..."

#--------------------------------------------------------------------------------------------------------------------------------
# author:           Paweł Karp       
# description:      install package with apt-get
#
# version
#--------------------------------------------------------------------------------------------------------------------------------
#   1.0 / 2015-12-18
#       -   start
#--------------------------------------------------------------------------------------------------------------------------------



#--------------------------------------------------------------------------------------------------------------------------------
# In:
#	$1    <-  package name
#	$2    <-  "quiet" - make quiet install
# Out:
#   0     -> package installed  
#   1     -> package not installed  
#   2     -> package is already installed  
#--------------------------------------------------------------------------------------------------------------------------------
installPackage()
{

    if [ $(dpkg-query -W -f='${Status}' $1 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
    
        if [ -z "$2" ]; then # No command-line arguments
            apt-get install -y --no-install-recommends $1 # if no argument - make normall install
        else 
            if [ $2 == "quiet" ]; then
                apt-get install -qq -y --no-install-recommends $PAK >/dev/null 2>&1
            else
                apt-get install -y --no-install-recommends $1
            fi
        fi
    
        if [ $? == 0 ]; then
            return 0
        else
            return 1
        fi
    else
        return 2
    fi
   
}



