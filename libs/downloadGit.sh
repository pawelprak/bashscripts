#!/bin/bash
scriptName=$(basename $BASH_SOURCE) 
#echo "load: $scriptName ..."

#--------------------------------------------------------------------------------------------------------------------------------
# author:           Paweł Karp       
# description:      download repository from git
#
# version
#--------------------------------------------------------------------------------------------------------------------------------
#   1.0 / 2016-01-21
#       -   start
#--------------------------------------------------------------------------------------------------------------------------------





#--------------------------------------------------------------------------------------------------------------------------------
# In:
#	$1    <-  www repository adress
#	$2    <-  download directory
# Out:
#   0     -> repository downloaded  
#   1     -> repository not downloaded - error
#   2     -> repository is already downloaded  
#--------------------------------------------------------------------------------------------------------------------------------
downloadGit()
{

    if [ ! -d "$2" ]; then # katalog nie istnieje
        mkdir -p $2
        git clone $1 $2

        if [ $? -ne 0 ]; then 
            return 1
        else
            return 0
        fi

    else
        return 2
    fi

}