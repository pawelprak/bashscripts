# UWAGA - stara wersja

Wszystkie etapy tworzenia systemu od nowa ale bez pomocy skryptów.  
Wszytkie polecenia klepane ręcznie na raspberry.  


### Zmienić rozmiar partycji na karte 2GB
`$ fdisk -l`  
`$ fdisk /dev/mmcblk0`  

`p`  
`d 2`  
`n p 2 125056`  
`p`  
`w`  

`$ reboot`  
`$ resize2fs /dev/mmcblk0p2`  
`$ df -h` 


### config.txt  
Przegrać plik config.txt do katalogu /boot/  



### Pierwsze instalacje  
`$ apt-get update`  
`$ apt-get install mc`  
`$ apt-get install nano`  
`$ apt-get install sudo`  



### Stworzenie nowego konta użytkownika  
`$ adduser zzm` - nadać hasło "wihajster1234"  /WAŻNE - tak ma być, to jest to samo co na DC1.   
`$ usermod -aG sudo zzm`  - nadanie uprawnień administratora.  
LUB:  
`$ visudo` - otworzy się edytor, pod uprawnieniami dla root wpisać nową linię dla użytkownika zzm:  

>zzm ALL=(ALL:ALL) ALL   
zzm ALL=NOPASSWD: /sbin/shutdown  
zzm ALL=NOPASSWD: /sbin/passwd    
zzm ALL=NOPASSWD: /bin/date    
zzm ALL=NOPASSWD: /usr/bin/node  

następnie wyjść z edytora i zapisać zmiany (CTRL-X / Y / enter)



### Autologin  
`$ cd /etc/systemd/system/getty@tty1.service.d/`  
`$ touch autologin.conf`  - stworzyć plik  

Wpisać do pliku:
>[Service]  
ExecStart=  
ExecStart=-/sbin/agetty --autologin zzm --noclear %I 38400 linux  

Uwaga: "zzm" - można wpisać innego użytkownika, np root  

W terminalu wykonać komendy:
`$ systemctl enable getty@tty1.service`  
`$ reboot`  







### Midori  
`$ apt-get install xinit`  
`$ apt-get install x11-xserver-utils` You might need to install x11-xserver-utils in order to make the xset -dpms and xset s off to work correctly.   
`$ apt-get install midori`  
`$ apt-get install matchbox

Przegrać autostart.sh do /home/zzm/  
W pliku /etc/rc.local dodać przed `exit 0` wpis:  
`sudo xinit ./home/zzm/autostart.sh &`  



### Reszta aplikacji    
`$ sudo apt-get install xdotool`  
`$ sudo apt-get install unclutter`  
`$ sudo apt-get install htop`   
`$ sudo apt-get install nginx`   



### Instalacja dodatkowych bibliotek  
`$ sudo apt-get install ttf-liberation` - Arial i inne.  


### Instalacja serwera ntp  
`$ sudo apt-get install ntp`   
`$ sudo apt-get install ntpdate`   


### Instalacja obsługi skryptów ftp   
`$ sudo apt-get install ncftp`  


### Instalacja serwera vnc
`$ sudo apt-get install x11vnc`  


### Instalacja dos2unix   
`$ sudo apt-get install dos2unix`  - program do konwersji plików tekstowych z microsoftowskim znakiem zakończenia lini itp.   


### Instalacja edytora plików
`$ sudo apt-get install gedit`   


### Instalacja node
`$ wget http://node-arm.herokuapp.com/node_latest_armhf.deb`  
`$ sudo dpkg -i node_latest_armhf.deb`  
`$ node -v`     


### Wyłączenie fsck  
w pliku zmienić cyfry w ostatniej kolumnie na 0 (kolejność wykonywania fsck)
>/etc/fstab  



### Dodatki
w /boot/cmdline.txt dodać na końcu : `quiet logo.nologo`  


### Podkręcenie zegara procesora  
`$ sudo apt-get install cpufrequtils`  

`sudo cpufreq-set --g performance`  - to nie działa po reboocie!   

Stworzyć plik:
`$ touch /etc/default/cpufrequtils`  , wpisać do niego:

> GOVERNOR="performance"  



### rtc
`$ sudo apt-get install i2c-tools`  

Do /etc/modules dodać wpisy:  
> i2c-dev  
i2c-bcm2708  


<!--
### Log files and RAMFS  
/etc/fstab:   
>none	/tmp	    tmpfs     size=4M,noatime	0 0   
none	/var/log	tmpfs     size=4M,noatime	0 0   
none	/var/run	tmpfs	  size=4M,noatime	0 0   
-->


###  Changing the /boot partition to read-only
/etc/fstab:   
>/dev/mmcblk0p1 /boot vfat defaults,ro  0 2
   

### prevent fsck from running at boot
`$ sudo tune2fs -c -1 -i 0 /dev/mmcblk0p1`   // to nie działa 
`$ sudo tune2fs -c -1 -i 0 /dev/mmcblk0p2`  



### Now we have to remove the swap:  
`$ sudo update-rc.d -f dphys-swapfile remove`  
`$ sudo swap-off all`  // to nie działa 
`$ sudo rm /var/swap`  // nie ma takiego katalogu


### Finally, we disable man indexing:  
Edit /etc/cron.weekly/man-db and /etc/cron.daily/man-db   
and put an exit 0 command as the first line.  
























