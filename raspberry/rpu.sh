#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------
#- autor:    Paweł Karp       
#- opis:     Raspberry Pi Utilities - zestaw narzędzi do obsługgi RPi
#
# wersje:
#--------------------------------------------------------------------------------------------------------------------------------
#- v1.0 / 2016-03-31
#-      - rozpoczęcie prac
#--------------------------------------------------------------------------------------------------------------------------------
# TODO
#   - sprawdzenie hwclock
#   - powiększenie karty SD
#   - pokaznie użycia pamięci / kary SD
#   - help z komendami linuxa
#   - 
#   - przy powiekszenie partycji dodać dialog z potwierdzeniem czy na pewno uruchomić procedurę + info o obecnym rozmiarze
#   - zmiana readOnly w fstab
#   - przy wykonywaniu poleceń pokazać na kolorowo jak wygląda składnia basha
#--------------------------------------------------------------------------------------------------------------------------------



#--------------------------------------------------------------------------------------------------------------------------------
# Wylistowanie dostepnych opcji (po prostu help)
#--------------------------------------------------------------------------------------------------------------------------------
## Opcje:
##
##       -h     Pokaz dostepne opcje
##       -?     -||-
##       -v     Wersja programu
##       -t     Temperature procesora
##       -f     Aktualna czestotliwosc pracy procesora
##       -c     Show /boot/config.txt
##       -m     Wyczyszczenie logu z konsoli przeglądarki Midowi
##       -s     Ustawienie statycznego adresu IP: 192.168.3.31
##       -d     Ustawienie DHCP
##       -p     Powiększ partycje do rozmiaru skarty SD


#--------------------------------------------------------------------------------------------------------------------------------
# Deklaracje
#--------------------------------------------------------------------------------------------------------------------------------
help=$(grep "^## " "${BASH_SOURCE[0]}" | cut -c 4-) #ustalenie jak ma się zaczynać opis helpa jak i versji
version=$(grep "^#- "  "${BASH_SOURCE[0]}" | cut -c 4-)

NET_FILE=/etc/network/interfaces            # sciezka do pliku z ustawieniami adresu IP
HOME_DIR=/home/zzm/skryptyBash/raspberry    # miejsce gdzie skrypt zostaje wywołany (podany na sztywno ponieważ będzie potem podlinkowany do /usr/local/bin])
LIBS_DIR=/home/zzm/skryptyBash/libs          # miejsce umieszczenia bibliotek pomocniczych (podany na sztywno, powód j.w.)

ROOT_UID=0                              # Only users with $UID 0 have root privileges
E_XCD=86                                # Can't change directory?
E_NOINPUT=66
E_NOPERM=77



#--------------------------------------------------------------------------------------------------------------------------------
# Załadowanie bibliotek zewnętrznych
#--------------------------------------------------------------------------------------------------------------------------------
cd $LIBS_DIR
if [ `pwd` != "$LIBS_DIR" ]             # Not in LIBS_DIR?
then
    echo "Zla sciezka dla skryptow zewnetrznych: "
    echo $LIBS_DIR "!=" $(pwd)
exit $E_XCD
fi 

cd $HOME_DIR                            # powrót do katalogu domowego
source $LIBS_DIR/displayAlert.sh		# podrasowane wyświetlanie komunikatów na konsoli


#--------------------------------------------------------------------------------------------------------------------------------
# Sprawdzenie czy skrypt uruchomił użytkownik z uprawnieniami roota
#--------------------------------------------------------------------------------------------------------------------------------
if [ "$UID" -ne "$ROOT_UID" ] 
then 
    displayAlert "Uruchom skrypt jako root" "error" "rpu.sh"
	exit $E_NOPERM
fi



#--------------------------------------------------------------------------------------------------------------------------------
# Obsługa parametrow
#--------------------------------------------------------------------------------------------------------------------------------
opt_h() {
    echo "$help"
}

opt_?() {
    echo "$help"
}

opt_v() {
    echo "$version"
}

opt_t() {
    temp=sudo vcgencmd measure_temp
    echo $temp
    
    #temp=sudo cat /sys/class/thermal/thermal_zone0/temp
    #temp=45234
    #tempC=$((temp / 1000))
    #echo "Temperatura:" $tempC "C"
}

opt_f() {
    echo "CPU governors:"
    governors=sudo cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
    echo ""
    echo "CPU freqActual:"
    freq=sudo cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_cur_freq
    echo ""
    echo "CPU freqMin:"
    freqMin=sudo cat /sys/devices/system/cpu/cpu*/cpufreq/cpuinfo_min_freq
    echo ""
    echo "CPU freqMax:"
    freqMax=sudo cat /sys/devices/system/cpu/cpu*/cpufreq/cpuinfo_max_freq
}

opt_c() {
    int=sudo vcgencmd get_config int
    str=sudo vcgencmd get_config str
    echo "config.txt INT:"
    echo $int
    echo "config.txt STRING:"
    echo $str
}

opt_p() {
    clear;
    sdSize=$(df -h /tmp | tail -1 | awk '{print $4}')
    displayAlert "Powiekszam partycje do wartosci maksymalnej" "info" "fdisk"
    (echo p; echo d; echo 2; echo n; echo p; echo 2; echo 125056; echo; echo p; echo w) | sudo fdisk  /dev/mmcblk0
    displayAlert "Potrzebny restart systemu!" "warn" "fdisk"
    displayAlert "Po restarcie uruchom polecenie: \"resize2fs /dev/mmcblk0p2\" aby dokonczyc proces"  "warn" "fdisk"
    #sleep 5
    #sudo reboot
}

opt_m() {
   	echo "Czyszcze log midori"
    cat /dev/null > /home/zzm/kopex/log/midori.log
}

opt_s() {
    sudo cat /dev/null > $NET_FILE # wyczyszcznie pliku z ustawieniami
    sudo echo "auto lo" >> $NET_FILE # wpisanie nowej konfiguracji
    sudo echo "iface lo inet loopback" >> $NET_FILE
    sudo echo "auto eth0" >> $NET_FILE
    sudo echo "" >> $NET_FILE
    sudo echo "#iface eth0 inet dhcp" >> $NET_FILE
    sudo echo "iface eth0 inet static" >> $NET_FILE
    sudo echo "" >> $NET_FILE
    sudo echo "address 192.168.3.31" >> $NET_FILE
    sudo echo "netmask 255.255.255.0" >> $NET_FILE
    sudo echo "gateway 192.168.3.100" >> $NET_FILE
    displayAlert "Ustawiono adres: 192.168.3.31" "ok" "static IP"
    displayAlert "Potrzebny restart systemu!" "warn" "static IP"
}

opt_d() {
    sudo cat /dev/null > $NET_FILE # wyczyszcznie pliku z ustawieniami
    sudo echo "auto lo" >> $NET_FILE # wpisanie nowej konfiguracji
    sudo echo "iface lo inet loopback" >> $NET_FILE
    sudo echo "auto eth0" >> $NET_FILE
    sudo echo "" >> $NET_FILE
    sudo echo "iface eth0 inet dhcp" >> $NET_FILE
    sudo echo "#iface eth0 inet static" >> $NET_FILE
    sudo echo "" >> $NET_FILE
    sudo echo "#address 192.168.3.31" >> $NET_FILE
    sudo echo "#netmask 255.255.255.0" >> $NET_FILE
    sudo echo "#gateway 192.168.3.100" >> $NET_FILE
    displayAlert "Ustawiono DHCP" "ok" "dhcp"
    displayAlert "Potrzebny restart systemu!" "warn" "dhcp"
}








#--------------------------------------------------------------------------------------------------------------------------------
# Start skryptu - sprawdzenie parametrow z wywolania
#--------------------------------------------------------------------------------------------------------------------------------
echo ""
while getopts "hv?tfcpmsd" opt; do
    eval "opt_$opt"
done
echo ""























