#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------
#- autor:    Paweł Karp       
#- opis:     Logowanie temperatury
#
# wersje:
#--------------------------------------------------------------------------------------------------------------------------------
#- v1.0 / 2016-04-01
#-      - rozpoczęcie prac
#--------------------------------------------------------------------------------------------------------------------------------



#--------------------------------------------------------------------------------------------------------------------------------
# Deklaracje
#--------------------------------------------------------------------------------------------------------------------------------
HOME_DIR=$(pwd)                         # miejsce gdzie skrypt zostaje wywołany
LIBS_DIR=/home/zzm/skryptyBash/libs                    # miejsce umieszczenia bibliotek pomocniczych
LOG_FILE=/home/zzm/kopex/log/temp.log  # lokalizacja pliku na raspberry
#LOG_FILE=$(pwd)/temp/t.log              # lokazlizacja plik do testow na laptopie

ROOT_UID=0                              # Only users with $UID 0 have root privileges
E_XCD=86                                # Can't change directory?
E_NOINPUT=66
E_NOPERM=77


#--------------------------------------------------------------------------------------------------------------------------------
# Załadowanie bibliotek zewnętrznych
#--------------------------------------------------------------------------------------------------------------------------------
cd $LIBS_DIR
if [ `pwd` != "$LIBS_DIR" ]             # Not in LIBS_DIR?
then
    echo "Zła ścieżka dla skryptów zewnętrznych: $LIBS_DIR != $(pwd)"
exit $E_XCD
fi 

cd $HOME_DIR                            # powrót do katalogu domowego
source $LIBS_DIR/logToFile.sh		    # logowanie do pliku
source $LIBS_DIR/displayAlert.sh		# podrasowane wyświetlanie komunikatów na konsoli

#--------------------------------------------------------------------------------------------------------------------------------
# Sprawdzenie czy skrypt uruchomił użytkownik z uprawnieniami roota
#--------------------------------------------------------------------------------------------------------------------------------
if [ "$UID" -ne "$ROOT_UID" ] 
then 
    displayAlert "Uruchom skrypt jako root" "error" "logtemp.sh"
	exit $E_NOPERM
fi


#--------------------------------------------------------------------------------------------------------------------------------
# Cykliczna Rejestracja temperatury
#--------------------------------------------------------------------------------------------------------------------------------
if [ ! -e "$LOG_FILE" ]; then # sprawdzenie czy plik istnieje (musi byc stworzony aby zadzialalo polecenie stat)
    logToFile "rozpoczęcie logowania" $LOG_FILE "start" # stworzenie nowego pliku
fi
 
while : ; do
    size=$(stat -c%s "$LOG_FILE")
    #echo $size
    
    if [ $size -lt 5000000 ] # lt - less then ; 5MB
    then
        temp=$(sudo vcgencmd measure_temp) # zapis aktualnej temperatury
        logToFile $temp $LOG_FILE
    else
        logToFile "backup pliku" $LOG_FILE "start" # zbyt duzy rozmiar pliku, backup starego i rozpoczecie zapisu nowego
    fi
    
    sleep 60s # czestotliwosc pomiarow
done












